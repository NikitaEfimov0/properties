# Properties

Репозиторий 5 этапа марафона по Android-разработке.
Задания:
1) [Properties](https://gitlab.com/NikitaEfimov0/properties/-/blob/main/properties.kt)
2) [Lazy properties](https://gitlab.com/NikitaEfimov0/properties/-/blob/main/lazy_properties.kt)
3) [Delegates examples](https://gitlab.com/NikitaEfimov0/properties/-/blob/main/delegates_examples.kt)
4) [Delegate](https://gitlab.com/NikitaEfimov0/properties/-/blob/main/delegates.kt)

[Скриншот успешного прохождения тестов](https://gitlab.com/NikitaEfimov0/properties/-/blob/main/task5.png)
